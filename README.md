# numbersGtk

Version anglaise de nombresGtk : les nombres de 0 à 100 en anglais.

Cette version devrait être fonctionnelle. Reste à debugger. A tester.

Les logs sont situés dans le /home de l'user

```~/.primtux/numbersGtk/numbersGtk.log```

![screenshoot](./screenshoot1.png)

## Installation


### Depuis le paquet debian

L'installer

```$ wget https://framagit.org/CyrilleBiot/numbersgtk/-/blob/main/numbers-gtk_1.0_all.deb```

```# dpkg -r numbers-gtk_1.0_all.deb```

Le supprimer

```# dpkg -i numbers-gtk```


### Depuis le git

Juste cloner et lancer.


```git clone https://framagit.org/CyrilleBiot/numbersGtk.git```

```./source/numbersGtk.py```


## Dépendances

Les dépendances sont les suivantes. Le paquets debian les gère.

```python3-vlc, python3-gi```

## Fonts
Nécessite la police [alarm_clock.ttf] ( https://www.dafont.com/fr/alarm-clock.font )

Sera incluse dans le package debian

```fc-cache -f -v```

## TODO
Affichage des logs dans un textview

